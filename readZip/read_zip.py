#!/usr/bin/python3.8
# Fabian hbr <fabian.hbr@eduge.ch>
# 21.09.2020
# (c) CFPT
"""
Reads the metadata of a zip
"""

from enum import Enum


class Signature(Enum):
    """
    Enum for the zip signatures.
    """
    LOCAL_FILE_HEADER = 0x04034b50
    DATA_DESCRIPTOR = 0x08074b50
    CENTRAL_DIRECTORY_FILE_HEADER = 0x02014b50


FILE_PATH = "./screenshot.zip"
BIG_ENDIAN = "big"
LITTLE_ENDIAN = "little"


def main():
    """
    Main function.
    """
    print("Ouverture du fichier...")

    # Read the file and store the data
    zip_file = open(FILE_PATH, "rb")
    data = zip_file.read()

    offset = 0

    while True:
        signature = int.from_bytes(data[offset + 0:offset + 4], LITTLE_ENDIAN)
        if signature == Signature.LOCAL_FILE_HEADER:
            # Read local file header
            version_to_extract = int.from_bytes(
                data[offset + 4:offset + 6], LITTLE_ENDIAN)
            general_purpose_bit_flag = int.from_bytes(
                data[offset + 6:offset + 8], LITTLE_ENDIAN)
            compression_method = int.from_bytes(
                data[offset + 8:offset + 10], LITTLE_ENDIAN)
            last_modification_time = int.from_bytes(
                data[offset + 10:offset + 12], LITTLE_ENDIAN)
            last_modification_date = int.from_bytes(
                data[offset + 12:offset + 14], LITTLE_ENDIAN)
            crc_32 = int.from_bytes(
                data[offset + 14:offset + 18], LITTLE_ENDIAN)
            compressed_size = int.from_bytes(
                data[offset + 18:offset + 22], LITTLE_ENDIAN)
            uncompressed_size = int.from_bytes(
                data[offset + 22:offset + 26], LITTLE_ENDIAN)
            file_name_length = int.from_bytes(
                data[offset + 26:offset + 28], LITTLE_ENDIAN)
            extra_field_length = int.from_bytes(
                data[offset + 28:offset + 30], LITTLE_ENDIAN)
            start_extra_field = offset + 30 + file_name_length
            file_name = data[offset + 30:start_extra_field].decode("utf-8")
            end_extra = start_extra_field + extra_field_length
            has_data_descriptor = general_purpose_bit_flag & 0x08 != 0

            # Print local file header
            print(
                f"Local file header signature : {hex(local_file_header_signature)}")
            print(f"Version to extract : {version_to_extract}")
            print("General purpose bit flag : {0:b}".format(
                general_purpose_bit_flag))
            print(f"Compression method : {compression_method}")
            print(f"Last modification time : {last_modification_time}")
            print(f"Last modification date : {last_modification_date}")
            print(f"CRC-32 : {crc_32}")
            print(f"Compressed size : {humanbytes(compressed_size)}")
            print(f"Uncompressed size : {humanbytes(uncompressed_size)}")
            print(f"File name : {file_name}")
            print(f"Has data descriptor : {has_data_descriptor}")


def humanbytes(byte):
    """
    Return the given bytes as a human friendly KB, MB, GB, or TB string
    from: https://stackoverflow.com/questions/12523586/python-format-size-application-converting-b-to-kb-mb-gb-tb
    """
    byte = float(byte)
    kilo_byte = float(1024)
    mega_byte = float(kilo_byte ** 2)  # 1,048,576
    giga_byte = float(kilo_byte ** 3)  # 1,073,741,824
    tera_byte = float(kilo_byte ** 4)  # 1,099,511,627,776

    if byte < kilo_byte:
        return '{0} {1}'.format(byte, 'Bytes' if 0 == byte > 1 else 'Byte')
    if kilo_byte <= byte < mega_byte:
        return '{0:.2f} KB'.format(byte/kilo_byte)
    if mega_byte <= byte < giga_byte:
        return '{0:.2f} MB'.format(byte/mega_byte)
    if giga_byte <= byte < tera_byte:
        return '{0:.2f} GB'.format(byte/giga_byte)
    if tera_byte <= byte:
        return '{0:.2f} TB'.format(byte/tera_byte)
    return byte


if __name__ == "__main__":
    main()
