#!/usr/bin/python3.8
# Fabian hbr <fabian.hbr@eduge.ch>
# 28.09.2020
# (c) CFPT
"""
Reads the metadata of a tiff
"""

from enum import Enum


class DataTypes(Enum):
    """
    Data types of a tiff file.
    """
    BYTE = 1
    ASCII = 2
    SHORT = 3
    LONG = 4
    RATIONAL = 5
    SBYTE = 6
    UNDEFINED = 7
    SSHORT = 8
    SLONG = 9
    SRATIONAL = 10
    FLOAT = 11
    DOUBLE = 12


DATA_TYPES = [
    "BAD_TYPE",
    "BYTE",
    "ASCII",
    "SHORT",
    "LONG",
    "RATIONAL",
    "SBYTE",
    "UNDEFINED",
    "SSHORT",
    "SLONG",
    "SRATIONAL",
    "FLOAT",
    "DOUBLE",
]

TAGS = {
    256: "ImageWidth",
    257: "ImageLength",
    258: "BitsPerSample",
    259: "Compression",
    262: "PhotometricInterpretation",
    266: "FillOrder",
    273: "StripOffets",
    274: "Orientation",
    277: "SamplesPerPixel",
    278: "RowsPerStrip",
    279: "StripByteCounts",
    284: "PlanarConfiguration",
    296: "ResolutionUnit",
    338: "ExtraSamples",
    339: "SampleFormat",
    34675: "ICC Profile"
}

FILE_PATH = "./Lenna.tiff"
BIG_ENDIAN = "big"
LITTLE_ENDIAN = "little"


def main():
    """
    Main function.
    """
    print("Opening the file...")

    # Read the file and store the data
    tiff_file = open(FILE_PATH, "rb")
    data = tiff_file.read()

    # Read header
    file_byte_order = data[0:2].decode("utf-8")
    byte_order = BIG_ENDIAN if file_byte_order == "MM" else LITTLE_ENDIAN
    signature = int.from_bytes(data[2:4], byte_order)
    ifd_offset = int.from_bytes(data[4:8], byte_order)

    # Print header
    print(f"Byte order : {file_byte_order}, {byte_order}")
    print(f"Signature : {signature}")
    print(f"Offset of 0th IFD : {ifd_offset}")

    # Read IFD
    nbr_directory_entries = int.from_bytes(
        data[ifd_offset:ifd_offset + 2], byte_order)

    print(f"Number of Directory Entries {nbr_directory_entries}\n")

    directory_offset = ifd_offset + 2
    for i in range(0, nbr_directory_entries):
        # Read directory entry
        tag = int.from_bytes(
            data[directory_offset:directory_offset + 2], byte_order)
        data_type = int.from_bytes(
            data[directory_offset + 2:directory_offset + 4], byte_order)
        count = int.from_bytes(
            data[directory_offset + 4:directory_offset + 8], byte_order)

        # Print the begining of the entry
        print(f"Tag : {TAGS[tag]}, {tag}")
        print(f"Data type : {DATA_TYPES[data_type]}, {data_type}")
        print(f"Count : {count}")

        # Read and print the value of the entry
        if data_type == DataTypes.SHORT.value:
            value = int.from_bytes(
                data[directory_offset + 8:directory_offset + 10], byte_order)
            print(f"Value : {value}")
        elif data_type == DataTypes.LONG.value:
            value = int.from_bytes(
                data[directory_offset + 8:directory_offset + 12], byte_order)
            print(f"Value : {value}")

        # Print a new line
        print("")

        # Increment the offset
        directory_offset += 12


if __name__ == "__main__":
    main()
