#!/usr/bin/python3.8
import os
import struct

FILE_PATH = "./ex1/logo.bmp"
NEW_IMAGE_PATH = "./ex1/newLogo.bmp"


def main():
    print("Ouverture du fichier...")

    # Read the file and stores the data
    logo = open(FILE_PATH, "rb")
    data = logo.read()

    # Read the type and the size of the file
    bf_type = data[:0x2].decode("utf-8")
    bf_size = int.from_bytes(data[0x2:0x6], "little")

    # Read the width and the height
    bi_width = int.from_bytes(data[0x12:0x16], "little")
    bi_height = int.from_bytes(data[0x16:0x1A], "little")

    # Read the number of bits per pixel
    bi_bit_count = int.from_bytes(data[0x1C:0x1E], "little")

    # Read the size of the image data (without the headers)
    bi_size_image = int.from_bytes(data[0x22:0x26], "little")

    # Print the output
    print(f"Signature du fichier : {bf_type}")
    print(f"Taille totale du fichier en octets : {bf_size}")
    print(f"Largeur de l'image : {bi_width}")
    print(f"Hauteur de l'image : {bi_height}")
    print(f"Nombre de bits par pixel : {bi_bit_count}")
    print(f"Taille en octets des données de l’image : {bi_size_image}")

    print("\nCréation de l'image inversée...")

    # Check if theres a file name like the new image, and if yes, deletes it
    if(os.path.isfile(NEW_IMAGE_PATH)):
        os.remove(NEW_IMAGE_PATH)

    # Create the new image
    newImage = open(NEW_IMAGE_PATH, "wb")

    # Write the header of the image
    newImage.write(data[:0x37])

    # Inverting the colors of the image
    for i in range(0x37, bf_size, 3):
        # Read the color data
        blue = int.from_bytes(data[i:i + 1], "little")
        green = int.from_bytes(data[i + 1:i + 2], "little")
        red = int.from_bytes(data[i + 2:i + 3], "little")

        # Invert the color data
        inverted_blue = 255 - blue
        inverted_green = 255 - green
        inverted_red = 255 - red

        # Write the inverted color data
        newImage.write(inverted_blue.to_bytes(1, "little"))
        newImage.write(inverted_green.to_bytes(1, "little"))
        newImage.write(inverted_red.to_bytes(1, "little"))

    # Close the files
    newImage.close()
    logo.close()


if __name__ == "__main__":
    main()
