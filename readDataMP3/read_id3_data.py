#!/usr/bin/python3.8
# Fabian hbr <fabian.hbr@eduge.ch>
# 07.09.2020
# (c) CFPT
"""
Reads the ID3 data of the file.
"""

FILE_PATH = "./dreams_for_sale.mp3"
BIG_ENDIAN = "big"
LITTLE_ENDIAN = "little"
FRAME_IDS = {
    "AENC": "Audio encryption",
    "APIC": "Attached picture",
    "ASPI": "Audio seek point index",
    "COMM": "Comments",
    "COMR": "Commercial frame",
    "ENCR": "Encryption method registration",
    "EQU2": "Equalisation (2)",
    "ETCO": "Event timing codes",
    "GEOB": "General encapsulated object",
    "GRID": "Group identification registration",
    "LINK": "Linked information",
    "MCDI": "Music CD identifier",
    "MLLT": "MPEG location lookup table",
    "OWNE": "Ownership frame",
    "PRIV": "Private frame",
    "PCNT": "Play counter",
    "POPM": "Popularimeter",
    "POSS": "Position synchronisation frame",
    "RBUF": "Recommended buffer size",
    "RVA2": "Relative volume adjustment (2)",
    "RVRB": "Reverb",
    "SEEK": "Seek frame",
    "SIGN": "Signature frame",
    "SYLT": "Synchronised lyric/text",
    "SYTC": "Synchronised tempo codes",
    "TALB": "Album/Movie/Show title",
    "TBPM": "BPM (beats per minute)",
    "TCOM": "Composer",
    "TCON": "Content type",
    "TCOP": "Copyright message",
    "TDEN": "Encoding time",
    "TDLY": "Playlist delay",
    "TDOR": "Original release time",
    "TDRC": "Recording time",
    "TDRL": "Release time",
    "TDTG": "Tagging time",
    "TENC": "Encoded by",
    "TEXT": "Lyricist/Text writer",
    "TFLT": "File type",
    "TIPL": "Involved people list",
    "TIT1": "Content group description",
    "TIT2": "Title/songname/content description",
    "TIT3": "Subtitle/Description refinement",
    "TKEY": "Initial key",
    "TLAN": "Language(s)",
    "TLEN": "Length",
    "TMCL": "Musician credits list",
    "TMED": "Media type",
    "TMOO": "Mood",
    "TOAL": "Original album/movie/show title",
    "TOFN": "Original filename",
    "TOLY": "Original lyricist(s)/text writer(s)",
    "TOPE": "Original artist(s)/performer(s)",
    "TOWN": "File owner/licensee",
    "TPE1": "Lead performer(s)/Soloist(s)",
    "TPE2": "Band/orchestra/accompaniment",
    "TPE3": "Conductor/performer refinement",
    "TPE4": "Interpreted, remixed, or otherwise modified by",
    "TPOS": "Part of a set",
    "TPRO": "Produced notice",
    "TPUB": "Publisher",
    "TRCK": "Track number/Position in set",
    "TRSN": "Internet radio station name",
    "TRSO": "Internet radio station owner",
    "TSOA": "Album sort order",
    "TSOP": "Performer sort order",
    "TSOT": "Title sort order",
    "TSRC": "ISRC (international standard recording code)",
    "TSSE": "Software/Hardware and settings used for encoding",
    "TSST": "Set subtitle",
    "TXXX": "User defined text information frame",
    "UFID": "Unique file identifier",
    "USER": "Terms of use",
    "USLT": "Unsynchronised lyric/text transcription",
    "WCOM": "Commercial information",
    "WCOP": "Copyright/Legal information",
    "WOAF": "Official audio file webpage",
    "WOAR": "Official artist/performer webpage",
    "WOAS": "Official audio source webpage",
    "WORS": "Official Internet radio station homepage",
    "WPAY": "Payment",
    "WPUB": "Publishers official webpage",
    "WXXX": "User defined URL link frame"
}
NON_TEXT_IDS = {
    "PRIV",
    "APIC"
}

def main():
    """
    Main function.
    """
    print("Ouverture du fichier...")

    # Read the file and store the data
    music = open(FILE_PATH, "rb")
    data = music.read()

    # Read header
    file_identifier = data[:0x3].decode("utf-8")
    version_major = int.from_bytes(data[3:4], BIG_ENDIAN)
    version_minor = int.from_bytes(data[4:5], BIG_ENDIAN)
    flags = int.from_bytes(data[5:6], BIG_ENDIAN)
    size = unsynchsafe(int.from_bytes(data[6:11], BIG_ENDIAN))

    # Print header
    print(f"File identifier : {file_identifier}")
    print(f"Version : ID3v2.{version_major}.{version_minor}")
    print(f"Size : {humanbytes(size)}")
    print(f"Byte Size : {size}\n")

    # Read flags
    has_extended_header = flags & 0b01000000 != 0
    has_footer = flags & 0b00010000 != 0

    # Extended header
    extended_header_size = 0
    if has_extended_header:
        extended_header_size = unsynchsafe(
            int.from_bytes(data[11:15], LITTLE_ENDIAN))
        print(f"Extended header size : {humanbytes(extended_header_size)}\n")

    # Compute offset and read frame ID
    frame_offset = 10 + extended_header_size

    while True:
        frame_id = data[frame_offset:frame_offset + 4].decode("utf-8")
        # Check end of loop
        if frame_id not in FRAME_IDS:
            break
        print(f"frame id: {frame_id}")
        print(f"Nice frame: {FRAME_IDS[frame_id]}")

        # Compute offset and read frame size
        frame_offset += 4
        frame_size = unsynchsafe(int.from_bytes(
            data[frame_offset:frame_offset + 4], BIG_ENDIAN))
        print(f"Frame size: {frame_size}")

        # Compute offset and read frame flags
        frame_offset += 4
        frame_flags = int.from_bytes(
            data[frame_offset:frame_offset + 2], BIG_ENDIAN)

        # Compute offset and read data
        frame_offset += 2
        if frame_id not in NON_TEXT_IDS:
            frame_data = data[frame_offset:frame_offset +
                              frame_size].decode("utf-8")
            print(f"Data: {frame_data}")
        frame_offset = frame_offset + frame_size
        print("")


def unsynchsafe(num):
    """
    from: https://stuffivelearned.org/doku.php?id=misc:synchsafe
    """
    out = 0
    mask = 0x7f000000
    for i in range(4):
        out >>= 1
        out |= num & mask
        mask >>= 8
    return out


def humanbytes(byte):
    """
    Return the given bytes as a human friendly KB, MB, GB, or TB string
    from: https://stackoverflow.com/questions/12523586/python-format-size-application-converting-b-to-kb-mb-gb-tb
    """
    byte = float(byte)
    kilo_byte = float(1024)
    mega_byte = float(kilo_byte ** 2)  # 1,048,576
    giga_byte = float(kilo_byte ** 3)  # 1,073,741,824
    tera_byte = float(kilo_byte ** 4)  # 1,099,511,627,776

    if byte < kilo_byte:
        return '{0} {1}'.format(byte, 'Bytes' if 0 == byte > 1 else 'Byte')
    if kilo_byte <= byte < mega_byte:
        return '{0:.2f} KB'.format(byte/kilo_byte)
    if mega_byte <= byte < giga_byte:
        return '{0:.2f} MB'.format(byte/mega_byte)
    if giga_byte <= byte < tera_byte:
        return '{0:.2f} GB'.format(byte/giga_byte)
    if tera_byte <= byte:
        return '{0:.2f} TB'.format(byte/tera_byte)
    return byte


if __name__ == "__main__":
    main()
